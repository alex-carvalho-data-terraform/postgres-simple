# <img src="img/postgresql.png" alt="postgres" width="30" style="vertical-align: middle;"> | Postgres simple  

Postgres on a Docker container via Terraform.  

<img src="img/terraform.png" alt="terraform" width="90" style="vertical-align: middle;"> 
<img src="img/docker.png" alt="docker" width="90" style="vertical-align: middle;">
<img src="img/postgresql.png" alt="postgres" width="90" style="vertical-align: middle;">

## Quick summary  

A simple Terraform infrastructure with a single Docker container containing a Postgres server with a persistent volume.  
The idea is to destroy the Postgres container between executions, but keep its data at the volume for the next Postgres container created.  

[Environment description](ENVIRONMENT.md)

## Table of contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [Contact](#contact)

## Installation

All environment is built using Terraform.  
The installation part consist in running Terraform to create time consuming resources, like `docker images`, and resources that should persist in between environmnet executions like `docker volumes`.

### Installation dependencies

Required installed software not covered in this instructions:

- docker 26.1.3+
- terraform 1.8.4+

### Installation steps

#### 1. clone this repository

```bash
git clone git@gitlab.com:alex-carvalho-data-terraform/postgres-simple.git
```

#### 2. Run the Terraform build as desribed on the link bellow

- [build instructions](BUILD.md)

## Usage

The usage part consist in running another subset of Terraform scripts, that should be very fast to complete.  
Here we create stateless objects such as `docker containers` and `docker networks`.  

### 1. Run the terraform execution as described on the link bellow  

- [execution instructions](EXEC.md)

## Contributing

1. Fork the repository.
2. Create a new branch: `git checkout -b feature-name`.
3. Make your changes.
4. Push your branch: `git push origin feature-name`.
5. Create a pull request.

## Contact

alex carvalho - [linkedin.com/in/alex-carvalho-big-data](https://www.linkedin.com/in/alex-carvalho-big-data/)
