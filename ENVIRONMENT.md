# environment #

<img src="img/postgresql.png" alt="apache kafka" height="100" style="vertical-align: middle;"> 

## Quick summary  

<img src="img/terraform.png" alt="Terraform" height="30" style="vertical-align: middle;"> Terraform infrastructure containing: 

### image

- postgres (16.3-bookworm)

### volume

- postgres-data-vol  

volume that will be mapped to the following postgresql container directory:   
`/var/lib/postgresql/data`  
that stores the database files and make it persistent in between containers destruction and recreation.    


### network

- postgres-network

### containers <img src="img/docker.png" alt="docker" height="30" style="vertical-align: middle;">

- 1 <img src="img/postgresql.png" alt="postgresql" height="20" style="vertical-align: middle;"> [postgres](#postgres)


# container description #

## Postgres

<img src="img/postgresql.png" alt="postgresql" height="60" style="vertical-align: middle;">  

### software

- postgresql 16.3  
- debian 12 (bookworm)


### exposed ports (host:container)

- 5432:5432  

### password

- 123456  

Dummy password for test purposes.  
A stronger password is adviced.  
