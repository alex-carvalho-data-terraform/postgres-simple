terraform {
    required_providers {
      docker = {
        source = "kreuzwerker/docker"
        version = "3.0.2"
      }
    }
}

provider "docker" {
    host = "unix:///var/run/docker.sock"
}

resource "docker_image" "postgres" {
    name = "postgres:16.3-bookworm"
}

resource "docker_volume" "postgres_data_vol" {
    name = "postgres-data-vol"
}
