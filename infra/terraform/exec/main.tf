terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

resource "docker_network" "postgres_network" {
  name = "postgres-network"
}

resource "docker_image" "postgres_image" {
  name         = "postgres:16.3-bookworm"
  keep_locally = true
}

resource "docker_container" "postgres" {
  image = docker_image.postgres_image.image_id
  name  = "postgres"

  env = [
    "POSTGRES_PASSWORD=123456"
  ]

  ports {
    internal = 5432
    external = 5432
  }

  volumes {
    container_path = "/var/lib/postgresql/data"
    volume_name    = "postgres-data-vol"
  }

  networks_advanced {
    name = docker_network.postgres_network.name
  }

  healthcheck {
    test     = ["CMD", "pg_isready", "-q", "-d", "postgres", "-U", "postgres"]
    interval = "5s"
    timeout  = "5s"
    retries  = 25
  }
}