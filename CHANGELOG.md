

## [1.0.0] - 2024-05-28

_Initial release_

### Added

- Postgres single container Terraform environment (split into build and execution sets). 


[1.0.0]: https://gitlab.com/alex-carvalho-data-terraform/postgres-simple/-/tags/v1.0.0
