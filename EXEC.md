# <img src="img/terraform.png" alt="Terraform" height="30" style="vertical-align: middle;"> <img src="img/docker.png" alt="docker" height="30" style="vertical-align: middle;"> | execution

It's intended to be very fast, less than 1 min.   
Here are created and `docker containers` and `docker networks` and destroyed after usage.  

## 1. Configuration

1. Go to terraform config dir for execution  
   
```bash
cd infra/terraform/exec/
```

2. Initialize Terraform

```bash
terraform init
```

## 2. Deployment instructions

Execute the build.  

```bash
terraform apply
```

## 3. How to run tests

### <img src="img/terraform.png" alt="Terraform" height="30" style="vertical-align: middle;"> 3.1. Check terraform objects

```bash
terraform state list
```

*Expected output similar to:*

```bash
docker_container.postgres
docker_image.postgres_image
docker_network.postgres_network
```

### <img src="img/docker.png" alt="docker" height="20" style="vertical-align: middle;"> 3.2. Check docker networks

```bash
docker network ls
```

*Expected output similar to:*

```bash
NETWORK ID     NAME                 DRIVER    SCOPE
82d88eb126e6   postgres-network     bridge    local
```

### <img src="img/docker.png" alt="docker" height="20" style="vertical-align: middle;"> 3.3. Check docker containers

```bash
docker container ls
```

*Expected output similar to:*

```bash
CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS                    PORTS                      NAMES
cff1149a14d8   cff6b68a194a   "docker-entrypoint.s…"   About a minute ago   Up About a minute (healthy)   0.0.0.0:5432->5432/tcp   postgres
```

## 4. Undeploy instructions

### 4.1. Stop and remove docker containers and volumes

at `infra/kraft/terraform/exec/`  

```bash
terraform destroy
```